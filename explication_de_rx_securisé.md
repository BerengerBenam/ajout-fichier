﻿**EXPOSE :**

1. Présentation de UFW,GUFW,apparmor+ quelque cas d’utilisation
1. Les ACL avec CISCO

ACL Standard

ACL Etendues

-au minimum trois routeurs pour les ACL

Quelque exemple avec GNS3

-le format d’un datagramme ip



**PARTIE VPN ?**

C'est quoi un VPN ?

[](https://www.google.com/search?sxsrf=ALiCzsb1cbFQFsJJArquj9YR3JywmKke0Q:1653475690064&q=C%27est+quoi+un+VPN+?&tbm=isch&source=iu&ictx=1&vet=1&fir=3BnAr8dAkYJLdM%252CxYeDn2lHY8NNnM%252C_&usg=AI4_-kRgJP0QjZslgKf_9N9whFVcNFy-DA&sa=X&sqi=2&ved=2ahUKEwjGq5r5vPr3AhVCBs0KHTgGDdkQ9QF6BAgxEAE#imgrc=3BnAr8dAkYJLdM)

![Résultat de recherche d'images pour "definition vpn"](Aspose.Words.19f9ba66-c29c-43f2-a650-de4c7d54afb4.001.png)[
](https://www.google.com/search?sxsrf=ALiCzsb1cbFQFsJJArquj9YR3JywmKke0Q:1653475690064&q=C%27est+quoi+un+VPN+?&tbm=isch&source=iu&ictx=1&vet=1&fir=3BnAr8dAkYJLdM%252CxYeDn2lHY8NNnM%252C_&usg=AI4_-kRgJP0QjZslgKf_9N9whFVcNFy-DA&sa=X&sqi=2&ved=2ahUKEwjGq5r5vPr3AhVCBs0KHTgGDdkQ9QF6BAgxEAE#imgrc=3BnAr8dAkYJLdM)

[](https://www.google.com/search?sxsrf=ALiCzsb1cbFQFsJJArquj9YR3JywmKke0Q:1653475690064&q=C%27est+quoi+un+VPN+?&tbm=isch&source=iu&ictx=1&vet=1&fir=3BnAr8dAkYJLdM%252CxYeDn2lHY8NNnM%252C_&usg=AI4_-kRgJP0QjZslgKf_9N9whFVcNFy-DA&sa=X&sqi=2&ved=2ahUKEwjGq5r5vPr3AhVCBs0KHTgGDdkQ9QF6BAgxEAE#imgrc=3BnAr8dAkYJLdM)

Un réseau privé virtuel (**VPN**) est une connexion sécurisée et chiffrée entre deux réseaux ou entre un utilisateur individuel et un réseau. Les **VPN** vous permettent de vous cacher lorsque vous surfez sur le Web.

Un **VPN** (**Virtual Private Network**) est un type de réseau informatique qui permet la création de liens directs entre des ordinateurs distants. Côté fonctionnement, le **VPN** repose sur la création d'un tunnel (via un protocole d'encapsulation) entre **les** deux ordinateurs

**Qu’est-ce qu’un VPN et comment fonctionne-t-il ?**

Un VPN (réseau privé virtuel) est l’un des meilleurs outils pour protéger de votre confidentialité sur Internet. Un VPN chiffre votre connexion et vous permet de rester caché lorsque vous surfez, faites des achats et effectuez des opérations bancaires en ligne. Lisez cet article pour une définition complète des VPN, savoir comment ils fonctionnent et pourquoi vous en avez besoin.

**IPsec**  __(*I*__*nternet __P__rotocol __S__ecurity*__)__ se base sur une suite de protocole pour assurer la confidentialité, l’intégrité et l’authentification  au niveau de la couche 3 du modèle OSI

**IPsec** utilise principalement deux protocoles pour son fonctionnement : **AH** (**A**uthentification **H**eader) et **ESP** (**E**ncapsulating **S**ecurity **P**ayload),  est un **protocole** appartenant à la suite IPsec, permettant de combiner plusieurs services de sécurité : confidentialité, authentification et intégrité.

**Qu'est-ce que le protocole IKE ?**

` `Le **protocole IKE** (Internet Key Exchange) a pour mission de sécuriser une connexion. Avant qu'une transmission IPsec soit réalisable, **IKE** se charge d'authentifier les deux parties tentant de se connecter au réseau informatique sécurisé, en échangeant des clés partagées.

- IPsec utilise deux modes qui sont le **mode transport** et le **mode  Tunnel**l

Les normes **IPsec** définissent **deux modes** distincts d'opération **IPsec** : le **mode** Transport et le **mode** Tunnel. Ces **modes** n'ont aucune incidence sur le codage des paquets. Les paquets sont protégés par AH, ESP ou ces **deux** protocoles dans chaque **mode**.

Mode transport :

Mode Tunnel :

**Exo cours Prochain ?**

Comment mettre en place ipsec sous cisco et Linux et Strongswan

**AH :** **Authentication Header**, ce **protocole** permet d'assurer l'intégrité des données et donc à la réception, nous sommes sur que c'est bien la bonne personne qui a envoyé les données. Pour cela, à la manière d'un IDS système, les données sont hachées, le hash obtenu est ensuite placées dans l'en-tête **AH**.

**SUITE Cours :**

**Notion Des Ports :**

UDP : 4500 port, 500} UDP

ESP/TCP : 50 

AH/TCP : 51

**Let’s encrypt**

**Résumé de Ipsec :**
## **1 – Introduction au protocole IPSec**
Le protocole IPsec est l’une des méthodes permettant de créer des [VPN (réseaux privés virtuels)](http://www.frameip.com/vpn/), c’est-à-dire de relier entre eux des systèmes informatiques de manière sûre en s’appuyant sur un réseau existant, lui-même considéré comme non sécurisé. Le terme sûr a ici une signification assez vague, mais peut en particulier couvrir les notions d’intégrité et de confidentialité. L’intérêt majeur de cette solution par rapport à d’autres techniques (par exemple les tunnels SSH) est qu’il s’agit d’une méthode standard (facultative en [IPv4](http://www.frameip.com/entete-ip/), mais obligatoire en [IPv6](http://www.frameip.com/entete-ipv6/)), mise au point dans ce but précis, décrite par différentes RFCs, et donc interopérable. Quelques avantages supplémentaires sont l’économie de bande passante, d’une part parce que la compression des entêtes des données transmises est prévue par ce standard, et d’autre part parce que celui-ci ne fait pas appel à de trop lourdes techniques d’encapsulation, comme par exemple les [tunnels PPP](http://frameip.com/l2tp-pppoe-ppp-ethernet/) sur lien SSH. Il permet également de protéger des protocoles de bas niveau comme [ICMP](http://www.frameip.com/entete-icmp/) et [IGMP](http://www.frameip.com/entete-igmp/), RIP, etc… 

IPSec présente en outre l‘intérêt d‘être une solution évolutive, puisque les algorithmes de chiffrement et d‘authentification à proprement parler sont spécifiés séparément du protocole lui-même. Celle-ci a cependant l‘inconvénient inhérent de sa flexibilité et sa grande complexité rend son implémentation délicate. Les différents services offerts par le protocole IPSec sont détaillés dans ce document. Les formes pour combiner entre eux ces divers protocoles, ce que leur mise en œuvre est tenue de supporter et sont ensuite présentées. Les moyens de la gestion des clefs de chiffrement et signature sont étudiés ainsi que la problèmatique sur l‘interopérabilité associée est évoquée. Enfin, un aperçu rapide est donné sur quelques implémentations IPSec, en s‘intéressant essentiellement sur leur conformité aux spécifications.
## **2 – Les services offerts par IPsec**
### **2.1 – Les deux modes d’échange IPsec**
Une communication entre deux hôtes, protégée par IPsec, est susceptible de fonctionner suivant deux modes différents : le mode transport et le mode tunnel. La première offre essentiellement une protection aux protocoles de niveau supérieur, le second permet quant à lui d’encapsuler des datagrammes IP dans d’autres datagrammes IP, dont le contenu est protégé. L’intérêt majeur de ce second mode est qu’il rend la mise en place de passerelles de sécurité qui traitent toute la partie IPsec d’une communication et transmettent les datagrammes épurés de leur partie IPsec à leur destinataire réel réalisable. Il est également possible d’encapsuler une communication IPsec en mode tunnel ou transport dans une autre communication IPsec en mode tunnel, elle-même traitée par une passerelle de sécurité, qui transmet les datagrammes après suppression de leur première enveloppe à un hôte traitant à son tour les protections restantes ou à une seconde passerelle de sécurité.
### **2.2 – Les protocoles à la base d’IPsec**
#### ***2.2.1 – AH (authentication header)***
AH est le premier et le plus simple des protocoles de protection des données qui font partie de la spécification IPsec. Il est détaillé dans la [RFC 2402](http://www.frameip.com/rfc-2402-ip-authentication-header/). Il a pour vocation de garantir :

- L’authentification : les datagrammes IP reçus ont effectivement été émis par l’hôte dont l’adresse IP est indiquée comme adresse source dans les entêtes.
- L’unicité (optionnelle, à la discrétion du récepteur) : un datagramme ayant été émis légitimement et enregistré par un attaquant ne peut être réutilisé par ce dernier, les attaques par rejeu sont ainsi évitées.
- L’intégrité : les champs suivants du datagramme IP n’ont pas été modifiés depuis leur émission : les données (en mode tunnel, ceci comprend la totalité des champs, y compris les entêtes, du datagramme IP encapsulé dans le datagramme protégé par AH), version (4 en IPv4, 6 en IPv6), longueur de l’entête (en IPv4), longueur totale du datagramme (en IPv4), longueur des données (en IPv6), identification, protocole ou entête suivant (ce champ vaut 51 pour indiquer qu’il s’agit du protocole AH), adresse IP de l’émetteur, adresse IP du destinataire (sans source routing).

En outre, au cas où du source routing serait présent, le champ adresse IP du destinataire a la valeur que l’émetteur a prévu qu’il aurait lors de sa réception par le destinataire. Cependant, la valeur que prendront les champs type de service (IPv4), indicateurs (IPv4), index de fragment (IPv4), TTL (IPv4), somme de contrôle d’entête (IPv4), classe (IPv6), flow label (IPv6), et hop limit (IPv6) lors de leur réception n’étant pas prédictible au moment de l’émission, leur intégrité n’est pas garantie par AH.

L’intégrité de celles des options IP qui ne sont pas modifiables pendant le transport est assurée, celle des autres options ne l’est pas. 
Attention, AH n’assure pas la confidentialité : les données sont signées mais pas chiffrées. 

**SUITE COURS :**

Résumé vpn de niveau 3

VPN de type ssl

IPSEC=========èStrongswan

SSL/=========èOpenVPN

Wireguard=====èWireguard

# **Qu’est-ce qu’un VPN SSL ?**
Un VPN SSL est un réseau privé virtuel créé à l’aide du protocole SSL (**S**ecure **S**ockets **L**ayer**) pour** établir une connexion sécurisée et cryptée sur un réseau moins sécurisé, comme Internet.

Qu’est-ce qu’un VPN SSL ?

Les VPN SSL sont apparus en réponse à la complexité du cadre [IPsec (Internet Protocol Security)](https://www.f5.com/fr_fr/services/resources/glossary/internet-protocol-security-ipsec) et à l’incapacité à prendre en charge tous les utilisateurs finaux, en particulier les utilisateurs distants, depuis toutes les plateformes possibles. Un VPN SSL fournit généralement deux choses : un [accès à distance sécurisé](https://www.f5.com/fr_fr/services/resources/glossary/secure-remote-access) via un portail web, et un accès au niveau du réseau via un tunnel sécurisé par SSL entre le client et le réseau de l’entreprise. Le principal avantage d’un VPN SSL est la sécurité et la confidentialité des données.

Puisqu’un VPN SSL utilise des technologies et navigateurs Web standard, il permet un accès à distance sécurisé aux applications de l’entreprise pour tous les utilisateurs sans avoir à installer et maintenir un logiciel client distinct sur l’ordinateur de chaque utilisateur. La plupart des VPN SSL sont également capables d’intégrer plusieurs mécanismes d’authentification.

Les solutions offrant deux modes d’accès via un protocole établi et omniprésent (SSL) sont mieux à même de fournir aux utilisateurs finaux un accès aux ressources, quelle que soit leur plateforme. En déployant une telle solution sur une plateforme éprouvée et hautement évolutive telle que [BIG IP](https://www.f5.com/fr_fr/products/big-ip-services), les services informatiques peuvent faire évoluer à la fois la solution et les services d’infrastructure qu’elle nécessite.

**Pourquoi un VPN SSL est-il important ?**

Avec l’augmentation du télétravail, les VPN SSL sont essentiels pour connecter les employés aux applications de travail dont ils ont besoin, tout en permettant aux services informatiques de s’assurer que seuls les utilisateurs autorisés ont un accès. Les VPN SSL constituent un moyen sûr donné à votre personnel, vos sous-traitants et vos partenaires dans le monde entier la possibilité d’accéder à des informations sensibles depuis pratiquement n’importe quel ordinateur ou appareil. En outre, ils permettent aux services informatiques d’exercer un contrôle total et précis sur l’accès aux données. Les VPN SSL sont de plus en plus courants dans l’environnement professionnel et la courbe d’apprentissage pour les mettre en œuvre et les utiliser est minime.

**Synthèse :** dans la partie vpn ssl on constate deux protocoles qui interviennent nous avon le protocole SSL et TLS.

- TLS utilise trois version de protocole TLS v1.0 , TLS v1.1 et TLS v1.0 et TLS v1.2

Donc SSL et TLS ont évolué pour assurer la protection divers comme SMTP,POP et bien d’autre.

**-Principale de Fonctionnement de SSL/TLS**

\- Le but premier est évidement d’assurer le secret des données échangées entre le client et le serveur donc nous parlons ici de confidentialité.

-deuxième objectif est d’assuré l’authentification du serveur par le client.









